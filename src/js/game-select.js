import { GAMES_ID } from "./config.js";

export default class GameSelect extends HTMLElement {
	static get observedAttributes() {
		return ["game"];
	}
	get game() {
		return this.getAttribute("game");
	}
	set game(str) {
		this.setAttribute("game", str);
	}
	get games() {
		return GAMES_ID;
	}

	connectedCallback() {
		this.render();
	}
	onInput(event) {
		event.stopPropagation();
		this.game = event.target.value;
		const selectEvent = new CustomEvent("select", {
			bubbles: true,
			detail: this.game,
		});
		this.dispatchEvent(selectEvent);
	}
	render() {
		this.$select = document.createElement("select");
		this.$select.addEventListener("input", this.onInput.bind(this));

		this.games.map((gameId) => {
			const $option = document.createElement("option");
			$option.value = gameId;
			$option.innerText = gameId;
			if (gameId === this.game) {
				$option.selected = true;
			}
			this.$select.append($option);
		});
		this.append(this.$select);
	}
}

window.customElements.define("game-select", GameSelect);
