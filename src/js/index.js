window.MWC_MANUAL_DEFINE = true;

import "./matrix.js";
/* rtc also has old version of mwc */
import * as rtc from "@sctlib/rtc";
import GameElement from "./game-element.js";
import GameSelect from "./game-select.js";
import SnakeGame from "./snake-game.js";
import RtcGame from "./rtc-game.js";
/* import PongGame from "./pong-game.js"; */
