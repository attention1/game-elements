export default class PongGame extends HTMLElement {
  constructor() {
    super();
    this._shadowRoot = this.attachShadow({ mode: "open" });
    this._shadowRoot.innerHTML = `
            <style>
                #gameCanvas {
                    background-color: #000;
                }
            </style>
            <canvas id="gameCanvas"></canvas>
        `;

    this.ball = { x: 100, y: 100, dx: 2, dy: 2, radius: 10 };
    this.paddle = { width: 10, height: 75, x: 0, y: 0, dy: 2 };
    this.canvas = this._shadowRoot.querySelector("#gameCanvas");
    this.context = this.canvas.getContext("2d");
  }

  connectedCallback() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.paddle.y = this.canvas.height / 2;
    this.animate();
    window.addEventListener("keydown", this.movePaddle.bind(this));
  }

  movePaddle(e) {
    switch (e.code) {
      case "ArrowUp":
        this.paddle.y -= this.paddle.dy;
        break;
      case "ArrowDown":
        this.paddle.y += this.paddle.dy;
        break;
    }
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.beginPath();
    this.context.arc(
      this.ball.x,
      this.ball.y,
      this.ball.radius,
      0,
      Math.PI * 2
    );
    this.context.rect(
      this.paddle.x,
      this.paddle.y,
      this.paddle.width,
      this.paddle.height
    );
    this.context.fillStyle = "#fff";
    this.context.fill();
    this.context.closePath();
    this.ball.x += this.ball.dx;
    this.ball.y += this.ball.dy;
    if (
      this.ball.y + this.ball.dy > this.canvas.height - this.ball.radius ||
      this.ball.y + this.ball.dy < this.ball.radius
    ) {
      this.ball.dy = -this.ball.dy;
    }
    if (
      this.ball.x + this.ball.dx > this.canvas.width - this.ball.radius ||
      this.ball.x + this.ball.dx < this.ball.radius
    ) {
      if (
        this.ball.y > this.paddle.y &&
        this.ball.y < this.paddle.y + this.paddle.height
      ) {
        this.ball.dx = -this.ball.dx;
      } else {
        location.reload();
      }
    }
  }
}

window.customElements.define("pong-game", PongGame);
