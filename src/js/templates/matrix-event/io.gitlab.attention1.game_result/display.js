/* event */
/* '{
 *   "content": {
 *     "detail": {
 *       "ascii": "-------------------------\\n...",
 *       "state": {
 *         "containerSize": "476.5",
 *         "defaultSpeed": 100,
 *         "endSpeed": 95,
 *         "fieldHeight": 23,
 *         "fieldWidth": 23,
 *         "food": [
 *           4,
 *           9
 *         ],
 *         "score": 1,
 *         "snake": [
 *           [
 *             0,
 *             17
 *           ],
 *           [
 *             1,
 *             17
 *           ]
 *         ],
 *         "snakeSize": 20
 *       }
 *     },
 *     "game": "wr",
 *     "twitterResult": "1 🐍!"
 *   },
 *   "origin_server_ts": 1704190143162,
 *   "room_id": "!kfCBBAEkxlyIAcTmoQ:matrix.org",
 *   "sender": "@56544281545:matrix.org",
 *   "type": "io.gitlab.attention1.game_result",
 *   "unsigned": {
 *     "age": 1939496
 *   },
 *   "event_id": "$ClEir08WrpTFTzjNzPnsrmYVeHH_wT7sfui8Dj5er8A",
 *   "user_id": "@56544281545:matrix.org",
 *   "age": 1939496
 * }' */

/* the html element used in the markup (to re-use everywhere in custom elements) */
const htmlTagName = "game-result";

/* our template, to "display" the content of an event of this type */
const template = document.createElement("template");
template.innerHTML = `
	<${htmlTagName}></${htmlTagName}>
`;

/* the web components definition, to display a "media track" as we want it */
export class GameResultMatrixDisplay extends HTMLElement {
	get event() {
		return JSON.parse(this.getAttribute("event")) || {};
	}
	connectedCallback() {
		this.render();
		console.log(this.event);
	}
	render() {
		this.replaceChildren();
		const $doms = this.createDoms();
		this.append(...$doms);
	}
	createDoms() {
		const { content = {} } = this.event;
		const { game, message, twitterResult, detail = {} } = content;
		const { ascii } = detail;
		const $game = document.createElement(`${htmlTagName}-game`);
		$game.innerText = game?.toLowerCase() || "no-game";

		const $message = document.createElement(`${htmlTagName}-message`);
		$message.innerText = message || twitterResult; // legacy key

		/* not used because broken render */
		const $ascii = document.createElement(`${htmlTagName}-ascii`);
		$ascii.innerText = ascii;

		return [$game, $message];
	}
}
/* auto define the element, so not part of the main code, but can be
imported from many places */
if (!customElements.get(htmlTagName)) {
	customElements.define(htmlTagName, GameResultMatrixDisplay);
}

export default template;
