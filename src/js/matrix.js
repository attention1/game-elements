import mwc from "@sctlib/mwc";
import resultDisplay from "./templates/matrix-event/io.gitlab.attention1.game_result/display.js";

mwc.eventsManager.registerEventType("io.gitlab.attention1.game_result", {
	displayTemplate: resultDisplay,
});
mwc.defineComponents(mwc.componentDefinitions);

export default mwc;
