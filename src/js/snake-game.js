export default class SnakeGame extends HTMLElement {
  static get observedAttributes() {
    return ["speed", "is-running", "container-size"];
  }

  get gameState() {
    return {
      score: this.score,
      defaultSpeed: this.defaultSpeed,
      endSpeed: this.speed,
      containerSize: this.containerSize,
      snakeSize: this.snakeSize,
      fieldWidth: this.fieldWidth,
      fieldHeight: this.fieldHeight,
      snake: this.snake,
      food: this.food,
    };
  }

  get colorScheme() {
    const darkScheme = {
      bg: "black",
      fg: "white",
      border: "white",
      snake: "white",
      food: "#ff00ff",
    };
    const lightScheme = {
      bg: "white",
      fg: "black",
      border: "black",
      snake: "black",
      food: "#00ff00",
    };
    if (
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
    ) {
      return darkScheme;
    } else {
      return lightScheme;
    }
  }

  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.dirX = null;
    this.dirY = null;
    this.score = 0;
    this.defaultSpeed = parseInt(this.getAttribute("speed")) || 100;
    this.speed = this.defaultSpeed;
    this.borderSize = 10;
    const containerSize =
      parseInt(this.getAttribute("container-size")) ||
      Math.min(window.innerWidth, window.innerHeight);
    this.containerSize =
      containerSize < 800 ? containerSize * 0.8 : containerSize * 0.5;
    this.snakeSize =
      parseInt(this.getAttribute("container-size")) < this.containerSize || 20;

    this.lastFrameTime = 0;
    this.gameCanvas = document.createElement("canvas");
    this.gameCanvas.width = this.containerSize;
    this.gameCanvas.height = this.containerSize;
    this.gameCanvas.addEventListener("mousedown", this.handleMouse.bind(this));
    this.gameCanvas.addEventListener("touchstart", this.handleTouch.bind(this));
    this.gameCanvas.setAttribute("tabindex", "0");
    this.gameCanvas.addEventListener("focus", this.addKeydown.bind(this));
    this.gameCanvas.addEventListener("blur", this.removeKeydown.bind(this));
    this.gameContext = this.gameCanvas.getContext("2d");

    this.scoreDisplay = document.createElement("div");
    this.scoreDisplay.innerText = `Score: ${this.score}`;

    this.shadowRoot.appendChild(this.gameCanvas);
    this.shadowRoot.appendChild(this.scoreDisplay);

    this.updateFieldSize();

    this.snake = this.newSnake();
    this.food = null;

    this.drawStartScreen();
  }

  addKeydown() {
    this.gameCanvas.addEventListener("keyup", this.handleKey.bind(this));
  }

  removeKeydown() {
    this.gameCanvas.removeEventListener("keyup", this.handleKey.bind(this));
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "is-running") {
      this.isRunning = newValue === "true";
    }
  }

  updateFieldSize() {
    this.fieldWidth = Math.floor(
      (this.gameCanvas.width - this.borderSize) / this.snakeSize
    );
    this.fieldHeight = Math.floor(
      (this.gameCanvas.height - this.borderSize) / this.snakeSize
    );
  }

  handleKey(e) {
    e.preventDefault();
    e.stopPropagation();
    const keyMap = {
      ArrowUp: () => {
        if (this.dirY !== 1) {
          this.dirX = 0;
          this.dirY = -1;
        }
      },
      ArrowDown: () => {
        if (this.dirY !== -1) {
          this.dirX = 0;
          this.dirY = 1;
        }
      },
      ArrowLeft: () => {
        if (this.dirX !== 1) {
          this.dirX = -1;
          this.dirY = 0;
        }
      },
      ArrowRight: () => {
        if (this.dirX !== -1) {
          this.dirX = 1;
          this.dirY = 0;
        }
      },
    };

    const keyAction = keyMap[e.key];
    if (keyAction) {
      keyAction();
      this.startGame();
    }
  }

  isTurningBack(newDirX, newDirY) {
    if (this.snake.length >= 2) {
      const [nextHeadX, nextHeadY] = [
        this.snake[0][0] + newDirX,
        this.snake[0][1] + newDirY,
      ];
      const [secondSegmentX, secondSegmentY] = this.snake[1];
      return nextHeadX === secondSegmentX && nextHeadY === secondSegmentY;
    }
    return false;
  }

  handleMouse(event) {
    this.gameCanvas.focus();
    const rect = this.gameCanvas.getBoundingClientRect();
    const mouseX = event.clientX - rect.left;
    const mouseY = event.clientY - rect.top;

    const [headX, headY] = this.snake[0];
    const center_x = headX * this.snakeSize + this.snakeSize / 2;
    const center_y = headY * this.snakeSize + this.snakeSize / 2;

    const dx = mouseX - center_x;
    const dy = mouseY - center_y;

    let newDirX = 0;
    let newDirY = 0;

    if (Math.abs(dx) > Math.abs(dy)) {
      // Left or right direction
      newDirX = dx > 0 ? 1 : -1;
    } else {
      // Up or down direction
      newDirY = dy > 0 ? 1 : -1;
    }

    if (!this.isTurningBack(newDirX, newDirY)) {
      this.dirX = newDirX;
      this.dirY = newDirY;
    }
    if (!this.isRunning) {
      this.startGame();
    }
  }

  handleTouch(e) {
    this.handleMouse(e.touches[0]);
  }

  startGame() {
    if (!this.isRunning) {
      this.isRunning = true;
      this.dirX = 0;
      this.dirY = 0;
      this.speed = this.defaultSpeed; // Reset speed to initial value
      this.score = 0; // Reset the score when the game starts
      this.snake = this.newSnake(); // Generate a new snake position when the game starts
      this.food = this.newFood(); // Generate a new food position when the game starts
    }
    this.gameCanvas.focus();
    // Start the game loop using requestAnimationFrame
    this.lastFrameTime = performance.now();
    this.gameLoop();
  }

  gameLoop() {
    const currentTime = performance.now();
    const deltaTime = currentTime - this.lastFrameTime;

    if (deltaTime >= this.speed) {
      this.update();
      this.render();

      this.lastFrameTime = currentTime;
    }

    if (this.isRunning) {
      requestAnimationFrame(() => this.gameLoop());
    }
  }

  endGame() {
    cancelAnimationFrame(this.gameLoop);
    this.isRunning = false;
    this.dispatchEvent(
      new CustomEvent("result", {
        detail: {
          state: this.gameState,
          ascii: this.gameStateToAscii(this.gameState),
        },
      })
    );
    this.render();
  }

  newSnake() {
    const startX = Math.floor(Math.random() * (this.fieldWidth - 2)) + 1;
    const startY = Math.floor(Math.random() * (this.fieldHeight - 2)) + 1;
    const snake = [];
    const initialLength = Math.max(1, this.score); // Ensure at least one segment
    for (let i = 0; i < initialLength; i++) {
      snake.push([startX - i, startY]);
    }
    return snake;
  }

  newFood() {
    let newFoodPos;
    do {
      newFoodPos = [
        Math.floor(Math.random() * (this.fieldWidth - 2)) + 1,
        Math.floor(Math.random() * (this.fieldHeight - 2)) + 1,
      ];
    } while (
      this.snake.some(([x, y]) => x === newFoodPos[0] && y === newFoodPos[1])
    );
    return newFoodPos;
  }

  updateSnakePosition() {
    const [headX, headY] = this.snake[0];
    const newHeadX = headX + this.dirX;
    const newHeadY = headY + this.dirY;
    const newHead = [newHeadX, newHeadY];
    this.snake.unshift(newHead); // Add the new head to the front of the snake

    // Check if the snake has collided with the food
    if (newHeadX === this.food[0] && newHeadY === this.food[1]) {
      this.food = this.newFood();
      this.score += 1; // Increment the score when the snake eats food
      this.speed *= 0.95; // Increase the speed by 5%
    } else {
      this.snake.pop(); // Remove the tail if the snake hasn't eaten food
    }
  }

  update() {
    const nextHeadX = this.snake[0][0] + this.dirX;
    const nextHeadY = this.snake[0][1] + this.dirY;

    if (this.handleCollision(nextHeadX, nextHeadY)) {
      return;
    }

    this.updateSnakePosition();
    this.render();
  }

  render() {
    // Clear the canvas
    const colorScheme = this.colorScheme;
    this.gameContext.beginPath();
    this.gameContext.fillStyle = colorScheme.bg;
    this.gameContext.clearRect(
      0,
      0,
      this.gameCanvas.width,
      this.gameCanvas.height
    );

    // Draw game boundaries
    this.gameContext.strokeStyle = colorScheme.border;
    this.gameContext.lineWidth = this.borderSize;
    this.gameContext.strokeRect(
      this.borderSize / 2,
      this.borderSize / 2,
      this.gameCanvas.width - this.borderSize,
      this.gameCanvas.height - this.borderSize
    );

    // Draw snake
    this.snake.forEach(([x, y]) => {
      this.gameContext.fillStyle = colorScheme.snake;
      this.gameContext.fillRect(
        this.borderSize +
          (x * (this.gameCanvas.width - this.borderSize * 2)) / this.fieldWidth,
        this.borderSize +
          (y * (this.gameCanvas.height - this.borderSize * 2)) /
            this.fieldHeight,
        this.snakeSize,
        this.snakeSize
      );
    });

    // Draw food
    this.gameContext.beginPath();
    this.gameContext.fillStyle = colorScheme.food;
    const foodX =
      this.borderSize +
      (this.food[0] * (this.gameCanvas.width - this.borderSize * 2)) /
        this.fieldWidth;
    const foodY =
      this.borderSize +
      (this.food[1] * (this.gameCanvas.height - this.borderSize * 2)) /
        this.fieldHeight;
    this.gameContext.arc(
      foodX + this.snakeSize / 2,
      foodY + this.snakeSize / 2,
      this.snakeSize / 2,
      0,
      2 * Math.PI
    );
    this.gameContext.fill();

    // Update score display
    this.scoreDisplay.innerText = `Score: ${this.score}`;

    // Clear Game Over text if game has not ended
    if (!this.gameLoop && this.score > 0) {
      console.log("draw end");
      this.drawEndScreen();
    }
  }

  handleCollision(nextHeadX, nextHeadY) {
    const isOutOfBounds =
      nextHeadX < 0 ||
      nextHeadX >= this.fieldWidth ||
      nextHeadY < 0 ||
      nextHeadY >= this.fieldHeight;

    const isSelfCollision = this.snake
      .slice(1)
      .some(([x, y]) => x === nextHeadX && y === nextHeadY);

    if (isOutOfBounds || isSelfCollision) {
      this.endGame();
      return true;
    }
    return false;
  }

  drawStartScreen() {
    this.gameContext.font = "20px Arial";
    this.gameContext.fillStyle = this.colorScheme.fg;
    this.gameContext.textAlign = "center";
    this.gameContext.fillText(
      `start game (push)`,
      this.gameCanvas.width / 2,
      this.gameCanvas.height / 2
    );
  }
  drawEndScreen() {
    this.gameContext.font = "20px Arial";
    this.gameContext.fillStyle = this.colorScheme.fg;
    this.gameContext.textAlign = "center";
    this.gameContext.fillText(
      `Congratulations! You've eaten a total of ${this.score} food`,
      this.gameCanvas.width / 2,
      this.gameCanvas.height / 2
    );
  }

  gameStateToAscii(gameState) {
    // Initialize an empty ASCII grid with '.'
    const grid = Array(gameState.fieldHeight)
      .fill()
      .map(() => Array(gameState.fieldWidth).fill("."));

    // Mark the snake on the grid with '#'
    gameState.snake.forEach(([x, y], index) => {
      let snakeChar;
      if (index === 0) {
        snakeChar = "@";
      } else {
        snakeChar = "#";
      }
      grid[y][x] = snakeChar;
    });

    // Mark the food on the grid with 'O'
    const [foodX, foodY] = gameState.food;
    grid[foodY][foodX] = "O";

    // Create border for the game state
    const horizontalBorder = Array(gameState.fieldWidth + 2)
      .fill("-")
      .join("");
    const gridWithBorders = grid.map((line) => "|" + line.join("") + "|");
    gridWithBorders.unshift(horizontalBorder);
    gridWithBorders.push(horizontalBorder);

    // Convert the grid to a string and return it
    const ascii = gridWithBorders.join("\n");
    return ascii;
  }
}

window.customElements.define("snake-game", SnakeGame);
