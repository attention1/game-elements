const template = document.createElement("template");
template.innerHTML = `
	<output slot="logs"></output>
	<form slot="send">
		<fieldset>
			<legend>Send data (custom send)</legend>
			<textarea name="textarea"></textarea>
			<button type="submit">Send message to peer-b</button>
		</fieldset>
	</form>
`;

export default class RtcGame extends HTMLElement {
	get searchParams() {
		return this.getAttribute("search-params");
	}
	connectedCallback() {
		this.render();
	}
	render() {
		this.replaceChildren();
		const $rtcUser = this.createUser();
		const $menu = this.createMenu();

		this.append($rtcUser, $menu);
	}
	createUser() {
		const $rtcUser = document.createElement("rtc-user");
		$rtcUser.append(template.content.cloneNode(true));

		/* will be used to auto set "matrix-peers" and "signaling-methods",
			 from the URL searchParams of same name (see rtc-user docs) */
		if (this.searchParams) {
			$rtcUser.setAttribute("search-params", this.searchParams);
		}
		return $rtcUser;
	}
	createMenu() {
		const $menu = document.createElement("menu");
		const $links = [
			["#", "Clear hash params (always refresh the page after clicking)"],
			["#signaling-methods=matrix-user-device,copypaste", "signaling-methods"],
			[
				"#signaling-methods=matrix-user-device&matrix-peers=@user:server.tld",
				"matrix-peers",
			],
		].map(([href, text]) => {
			const $li = document.createElement("li");
			const $link = document.createElement("a");
			$link.setAttribute("href", href);
			$link.textContent = text;
			$li.append($link);
			return $li;
		});
		$menu.append(...$links);
		return $menu;
	}
}

window.customElements.define("rtc-game", RtcGame);
