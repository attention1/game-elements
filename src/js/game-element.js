import { NtfyApi } from "@sctlib/ntfy-elements";
import { GAMES_ID, GAMES_DATA } from "./config.js";
import mwc from "./matrix.js";

const { api: matrixApi } = mwc;

const ntfyApi = new NtfyApi(window.location.hostname);
const PUBLISH_NTFY = true; // only locally for native notifications
const PUBLISH_MATRIX = true; // publish results in game-element matrix room

export default class GameElement extends HTMLElement {
	static get observedAttributes() {
		return ["game"];
	}
	get game() {
		return this.getAttribute("game");
	}
	set game(str) {
		this.setAttribute("game", str);
	}
	get ntfyLocalServer() {
		/* https://stackoverflow.com/questions/45732092/is-127-0-0-165535-the-network-equivalent-of-dev-null */
		return "http://localhost:9";
	}
	get matrixEventFilter() {
		return {
			types: ["m.room.message", "io.gitlab.attention1.game_result"],
			not_types: [
				"m.room.redaction",
				"m.room.member",
				"libli.widget",
				"im.vector.modular.widgets",
				"im.vector.modular.widgets.libli.widget",
				"m.reaction",
				"m.room.history_visibility",
				"m.room.power_levels",
				"m.room.name",
				"m.room.topic",
				"m.space.child",
				"m.room.avatar",
			],
		};
	}
	constructor() {
		super();
		this.$player = self.crypto.randomUUID();
		this.$gameInterface = this.createGameInterface();
		this.$gameLogs = this.createSettingsPanel();
		this.$gameSelect = this.$gameInterface.querySelector("game-select");
		this.$gameContainer = this.$gameInterface.querySelector("game-container");
	}
	attributeChangedCallback(attrName, prevValue, nextValue) {
		if (["game"].includes(attrName)) {
			this.$gameContainer && this.renderGame();
		}
	}

	/* https://github.com/matrix-org/matrix-hookshot/blob/main/src/Connections/GenericHook.ts#L81-L108 (Apache License 2.0)*/
	sanitiseObjectForMatrixJSON(data, depth = 0) {
		const SANITIZE_MAX_DEPTH = 5;
		const SANITIZE_MAX_BREADTH = 25;
		if (typeof data === "number" && !Number.isInteger(data)) {
			return data.toString();
		}
		if (
			depth > SANITIZE_MAX_DEPTH ||
			typeof data !== "object" ||
			data === null
		) {
			return data;
		}
		if (Array.isArray(data)) {
			return data.map((d, i) =>
				i > SANITIZE_MAX_BREADTH
					? d
					: this.sanitiseObjectForMatrixJSON(d, depth + 1),
			);
		}
		let breadth = 0;
		const record = { ...data };
		for (const [key, value] of Object.entries(data)) {
			breadth++;
			if (breadth > SANITIZE_MAX_BREADTH) {
				break;
			}
			record[key] = this.sanitiseObjectForMatrixJSON(value, depth + 1);
		}
		return record;
	}

	requestNotifications() {
		/* check if we can use the notifications */
		(async () => {
			try {
				const permissions = await ntfyApi.notifier.navigatorPermission();
				if (!permissions) {
					await ntfyApi.notifier.requestNotificationPermission();
				} else {
					/* message, topic, hostname */
					/* await ntfyApi.nativeNotify(
						 { message: "Games & Notifications are on!!" },
						 "user",
						 ); */
				}
			} catch (e) {}
		})();
	}

	async onResult({ detail, target }) {
		const game = target.tagName;
		await ntfyApi.nativeNotify(
			{ message: `${game} result: ${detail.state.score}` },
			"user",
		);
		/* create the new matrix event with base info */
		if (matrixApi.authAny && PUBLISH_MATRIX) {
			/* matrix does not handle float numbers in JSON?!
				 https://gitlab.com/sctlib/mwc/-/issues/5 */
			const content = {
				detail: this.sanitiseObjectForMatrixJSON(detail),
				game,
				message: `${detail.state.score} 🐍!`,
			};

			const newMxEvent = {
				room_id: "!kfCBBAEkxlyIAcTmoQ:matrix.org",
				event_type: "io.gitlab.attention1.game_result",
				content,
			};
			try {
				const res = await matrixApi.sendEvent(newMxEvent);
				console.log("mx res", res);
			} catch (resError) {
				console.log("resError", resError);
				const { errcode, error } = resError;
				if (errcode === "M_FORBIDDEN") {
					await ntfyApi.nativeNotify({
						messsage:
							"Join the room (as guest or login user), to post scores (and chat) in the matrix room",
					});
				} else {
					console.log("matrixApi", newMxEvent, matrixApi);
				}
			}
		}
	}

	onMxContext({ detail }) {
		const gameEvents = detail.events.filter(
			({ type }) => type === "io.gitlab.attention1.game_result",
		);
		console.log("onMxContext", gameEvents);
	}
	onGameSelect({ detail }) {
		console.log("game select", detail);
		this.game = detail;
		this.renderGame();
		this.updateSearchParam("game", this.game);
	}
	updateSearchParam(name, value) {
		if (name) {
			const params = new URLSearchParams(window.location.search);
			console.log("params", params);
			if (value) {
				params.set(name, value);
			} else {
				params.delete(name);
			}
			const newUrl = new URL(window.location);
			newUrl.search = params;
			history.pushState({}, window.location.title, newUrl);
		}
	}

	connectedCallback() {
		this.requestNotifications();
		this.setInitialProps();
		this.game = this.game || GAMES_ID[0];
		this.$gameSelect.setAttribute("game", this.game);
		this.render();
	}
	setInitialProps() {
		const searchProps = ["game"];
		const params = new URLSearchParams(window.location.search);
		searchProps.forEach((prop) => {
			if (params.has(prop)) {
				const value = params.get(prop);
				this[prop] = value;
			}
		});
	}
	createGameInterface() {
		const $interface = document.createElement("game-interface");
		const $details = document.createElement("details");
		$details.setAttribute("open", true);
		const $summary = document.createElement("summary");

		const $gameSelect = document.createElement("game-select");
		$gameSelect.addEventListener("select", this.onGameSelect.bind(this));
		$summary.append($gameSelect);

		const $gameContainer = document.createElement("game-container");
		$details.append($summary, $gameContainer);
		$interface.append($details);
		return $interface;
	}
	createDetailInfo() {
		/* info section */
		const $detailsInfo = document.createElement("details");
		const $summaryInfo = document.createElement("summary");
		$summaryInfo.innerText = "Info";
		const $sectionInfo = document.createElement("section");
		$sectionInfo.innerHTML = `
			<p>
				Welcome to
				<a href="https://gitlab.com/attention1/game-elements"
					>@attention1/game-elements</a
				>!
			</p>
			<p>
				On this page you should be able to play web based games, and
				communicate results (and messages) on
				<a href="https://matrix.org/">matrix</a>, and maybe even connect with (WebRTC)peers.
			</p>
			<p>
				<i>Join</i> the room as guest, to chat & auto submit your game results, or sign-in to an existing matrix account.
			</p>
		`;
		$detailsInfo.append($summaryInfo, $sectionInfo);
		return $detailsInfo;
	}
	createDetailMatrix() {
		/* user section */
		const $detailsMatrix = document.createElement("details");
		const $summaryMatrix = document.createElement("summary");
		$summaryMatrix.innerText = "Chat & Join result room";

		const $matrixAuth = document.createElement("matrix-auth");
		$matrixAuth.setAttribute("show-guest", true);
		$matrixAuth.setAttribute("show-user", true);
		$matrixAuth.setAttribute("origin", "https://matrix.to/#");
		const $matrixRoom = document.createElement("matrix-room");
		$matrixRoom.setAttribute(
			"profile-id",
			"#attention1-game-results:matrix.org",
		);
		$matrixRoom.setAttribute("origin", "https://matrix.to/#");
		$matrixRoom.setAttribute("show-room-actions", true);
		$matrixRoom.setAttribute("show-event-info", true);
		$matrixRoom.setAttribute("show-event-sender", true);
		$matrixRoom.setAttribute("show-user-info", true);
		$matrixRoom.setAttribute("show-send-event", true);
		$matrixRoom.setAttribute(
			"send-event-types",
			JSON.stringify(["m.room.message"]),
		);
		$matrixRoom.setAttribute("show-context", true);
		$matrixRoom.setAttribute("context-limit", 10);
		$matrixRoom.setAttribute("filter", JSON.stringify(this.matrixEventFilter));
		$matrixRoom.addEventListener("mxcontext", this.onMxContext.bind(this));

		$detailsMatrix.append($summaryMatrix, $matrixAuth, $matrixRoom);
		return $detailsMatrix;
	}
	createSettingsPanel() {
		const $settingsPanel = document.createElement("game-settings");
		const $settingsDetails = document.createElement("details");
		const $settingsSummary = document.createElement("summary");
		$settingsSummary.innerText = "@?";
		const $detailsInfo = this.createDetailInfo();
		const $detailsMatrix = this.createDetailMatrix();
		$settingsDetails.append($settingsSummary, $detailsInfo, $detailsMatrix);
		$settingsPanel.append($settingsDetails);
		return $settingsPanel;
	}
	render() {
		this.append(this.$gameInterface, this.$gameLogs);
	}
	renderGame() {
		this.$gameContainer.innerHTML = "";
		const gameData = GAMES_DATA[this.game];
		if (gameData) {
			const $game = document.createElement(gameData.element);
			if (gameData.attributes) {
				Object.entries(gameData.attributes).map(([attrName, attrValue]) => {
					if (typeof attrValue === "object") {
						$game.setAttribute(attrName, JSON.stringify(attrValue));
					} else {
						$game.setAttribute(attrName, attrValue);
					}
				});
			}
			if (gameData.event) {
				$game.addEventListener(gameData.event, this.onResult.bind(this));
			} else {
				$game.addEventListener("result", this.onResult.bind(this));
			}
			this.$gameContainer.append($game);
		}
	}
}

window.customElements.define("game-element", GameElement);
