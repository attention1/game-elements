const GAMES_DATA = {
	"snake-game": {
		element: "snake-game",
	},
	"rtc-user": {
		element: "rtc-game",
		attributes: {
			"search-params": ["matrix-peers", "signaling-methods"],
		},
	},
};
const GAMES_ID = Object.entries(GAMES_DATA).map(([gamesId, gameData]) => {
	return gamesId;
});

export { GAMES_DATA, GAMES_ID };
