# game-elements

This is an experiemental platform to distribute web (components) based games,
and connect players as peers with [WebRTC](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API).

This project only involves frontend code running in the client's
browser. WebRTCPeerConnection(s) can be establised with various methods, without
the need of a predefined server; a user can decide how-to.

This project also tries to takes advantages of the
[Matrix.org](https://matrix.org/) ecosystem, as one of the signaling method
([send-to-device](https://spec.matrix.org/v1.7/client-server-api/#send-to-device-messaging)),
as well as by posting [game results, and chats, into matrix public
rooms](https://gitlab.com/sctlib/mwc) (project chat in
[#attention1-game-elements:matrix.org](https://app.element.io/#/room/#attention1-game-elements:matrix.org)).

## Usage

It is possible to play online, offline, develop on the project, re-use &
customize the project elsewhere.

## Play games

To play games [go on the website](https://attention1.gitlab.io/game-elements/)
and select a game in the game selection menu.

## Install game-elements in an other project

This project is written in HTML, CSS, Javascript, and distributed as a npm
package.

- https://www.npmjs.com/package/@attention1/game-elements
- install with `npm i @sctlib/game-elements`

# Objectives

- play with creating "simple web based games"
- play with web-rtc peer connections
- try to create multiplayer games (and game validation, no noone can cheat?!)
- play with [rtc-peer signaling without server](https://gitlab.com/sctlib/rtc)

# Development

- `npm i`
- `npm run dev`

Code is Javascript, HTML, CSS, [web-components written with lit](https://lit.dev/).

## Concepts

These tutorials explain a lot of the concepts for WebRTC multiplayer game.
We don't use their services and tools, but the content of the tuto is cool.

- https://www.construct.net/en/tutorials/multiplayer-tutorial-concepts-579
- https://www.construct.net/en/tutorials/multiplayer-tutorial-chat-room-591
- https://www.construct.net/en/tutorials/multiplayer-tutorial-pong-626?vic=12

## Project ideas

Games could be anything in the context of this app, when a game is selected from
the project's UI `<select/>` element, a [web-component with the same name is
rendered](./src/js/game-element.js) (see `#renderGame()`).

The game should be an existing web-component/custom-element (otherwise it will
be an undefined custom HTML element, with no feature). All known [games are
defined in the
config](https://gitlab.com/attention1/game-elements/-/blob/main/src/js/config.js)
file.

> This is all experiemental and for research and enjoyment

## Todo directions

- be able to gather webrtc peers, and play game together
- be able with peers, to "customize" a game room (like a matrix room), with widgets (games, video, audio)
- connect peers in network (mesh etc.)
- be able to create "teams of peers"
- (lazy) import (and define) games as javascript modules (and web-components)
- improve game management / definitions / events, so it can support "many
  games", and each can be added easily
- improve concept of a "user" so they can customize their gaming experience,
  avaiable games; maybe even add games (matrix-rooms, rtc distribution)
